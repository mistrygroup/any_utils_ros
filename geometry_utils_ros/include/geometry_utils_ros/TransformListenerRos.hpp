/**
 * @authors     Gabriel Hottiger
 * @affiliation ANYbotics
 * @brief       TF2 implementation of the transform listener abstraction.
 */

#pragma once

#include <tf2_ros/transform_listener.h>

#include <any_measurements_ros/any_measurements_ros.hpp>
#include <geometry_utils/TransformListener.hpp>

#include "geometry_utils_ros/typedefs.hpp"

namespace geometry_utils_ros {

class TransformListenerRos : public geometry_utils::TransformListener {
 public:
  //! Constructor
  TransformListenerRos() : geometry_utils::TransformListener(), buffer_{}, listener_{buffer_} {}

  //! Default destructor
  ~TransformListenerRos() override = default;

  //! @copydoc geometry_utils::TransformListener::getCurrentTime
  Time getCurrentTime() const override { return any_measurements_ros::fromRos(ros::Time::now()); }

  //! @copydoc geometry_utils::TransformListener::canTransform
  bool canTransform(const std::string& targetFrame, const std::string& sourceFrame, const Time& time) const override;

  //! @copydoc geometry_utils::TransformListener::getTransformation
  bool getTransformation(TransformStamped* transformStamped, const std::string& targetFrame, const std::string& sourceFrame,
                         const Time& time, double timeout) const override;

 private:
  //! TF Buffer
  tf2_ros::Buffer buffer_;
  //! TF Transform listener
  tf2_ros::TransformListener listener_;
};

}  // namespace geometry_utils_ros
