# ANY Utils ROS

## Overview

Collection of ROS-based utilities.

The contained packages have been tested under ROS Melodic and Ubuntu 18.04. This is research code, expect that it changes often and any fitness for a particular purpose is disclaimed.

The source code is released under a [BSD 3-Clause license](LICENSE).

Contact: leggedrobotics@ethz.ch

## Building

[![Build Status](https://ci.leggedrobotics.com/buildStatus/icon?job=bitbucket_leggedrobotics/any_utils_ros/master)](https://ci.leggedrobotics.com/job/bitbucket_leggedrobotics/job/any_utils_ros/job/master/)

In order to install, clone the latest version from this repository into your catkin workspace and compile the packages.

## Usage

Please report bugs and request features using the [Issue Tracker](https://bitbucket.org/leggedrobotics/any_utils_ros/issues).

## Packages

This is only an overview. For more detailed documentation, please check the packages individually.

### bageditor

Load bag files into an std::vector.

### center_frame_tool

Plugin for centering the camera on a specified frame.

### interaction_marker

Interactive RViz marker using plugins for interactions.

### pose_tf_publisher

Publish tf messages using the data of an incoming pose topic.

### rviz_world_loader

Tool to load a world consisting of meshes from a file and publish its elements as markers.

### tf_to_pose_converter

Tool which listens to tf and publishes a certain transformation as pose.
Useful e.g. for logging or plotting a specific transformation.

### pinger_ros

Tool which pings a given PC at a given rate. The time delay is published as a 
Float64Stamped. If ping fails the delay is published as -1.