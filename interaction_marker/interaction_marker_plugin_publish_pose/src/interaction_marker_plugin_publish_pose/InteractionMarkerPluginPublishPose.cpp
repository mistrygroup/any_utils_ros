// interaction marker
#include "interaction_marker/InteractionMarker.hpp"

// interaction marker icp
#include "interaction_marker_plugin_publish_pose/InteractionMarkerPluginPublishPose.hpp"


namespace interaction_marker_plugin_publish_pose {


InteractionMarkerPluginPublishPose::InteractionMarkerPluginPublishPose()
{

}

InteractionMarkerPluginPublishPose::~InteractionMarkerPluginPublishPose()
{

}

void InteractionMarkerPluginPublishPose::initializePlugin(const yaml_tools::YamlNode parameters)
{
  // Get parameters.
  poseFrameId_ = parameters["pose_frame_id"].as<std::string>();
  const std::string entryText = parameters["entry_text"].as<std::string>();

  // Publishers.
  posePublisher_ = interactionMarker_->getNodeHandle().advertise<geometry_msgs::PoseStamped>(
      parameters["publishers"]["pose"]["topic"].as<std::string>(),
      parameters["publishers"]["pose"]["queue_size"].as<int>(),
      parameters["publishers"]["pose"]["latch"].as<bool>());

  // Add module menu entry.
  interactionMarker_->getMenuHandler()->insert(entryText, boost::bind(&InteractionMarkerPluginPublishPose::publishPose, this, _1));
}

void InteractionMarkerPluginPublishPose::publishPose(const visualization_msgs::InteractiveMarkerFeedbackConstPtr& feedback)
{
  if (posePublisher_.getNumSubscribers() == 0 &&
      !posePublisher_.isLatched())
    return;

  geometry_msgs::PoseStamped poseStamped;
  poseStamped.header.stamp = ros::Time::now();
  poseStamped.header.frame_id = feedback->header.frame_id;
  poseStamped.pose = feedback->pose;
  if (!interactionMarker_->transformPose(poseStamped, poseFrameId_))
    return;

  posePublisher_.publish(poseStamped);
}


} // interaction_marker_plugin_publish_pose


PLUGINLIB_EXPORT_CLASS(interaction_marker_plugin_publish_pose::InteractionMarkerPluginPublishPose, interaction_marker::InteractionMarkerPluginBase)

