#pragma once


// ros
#include <ros/ros.h>
#include <visualization_msgs/InteractiveMarkerFeedback.h>

// pluginlib
#include <pluginlib/class_list_macros.h>
#include <pluginlib/class_loader.h>

// param io
#include <param_io/get_param.hpp>

// yaml tools
#include <yaml_tools/YamlNode.hpp>


namespace interaction_marker {

// Forward declaration of the InteractionMarker.
class InteractionMarker;

/*!
 * This class is the base class of all interaction marker plugins.
 */
class InteractionMarkerPluginBase
{
protected:
  InteractionMarker* interactionMarker_ = nullptr;

public:
  InteractionMarkerPluginBase();
  virtual ~InteractionMarkerPluginBase();

  /*!
   * This function will be called after the constructor.
   * @param interactionMarker  pointer to the interaction marker this plugin belongs to.
   */
  void initializeBase(InteractionMarker* interactionMarker);

  /*!
   * This function will be called after initializeBase(..).
   * @param parameters         yaml node containing plugin specific parameters.
   */
  virtual void initializePlugin(const yaml_tools::YamlNode parameters) = 0;
};


} // interaction_marker

