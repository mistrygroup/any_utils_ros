// interaction marker
#include "interaction_marker/InteractionMarker.hpp"
#include "interaction_marker/InteractionMarkerPluginBase.hpp"


namespace interaction_marker {


InteractionMarkerPluginBase::InteractionMarkerPluginBase()
{

}

InteractionMarkerPluginBase::~InteractionMarkerPluginBase()
{

}

void InteractionMarkerPluginBase::initializeBase(InteractionMarker* interactionMarker)
{
  interactionMarker_ = interactionMarker;
}


} // interaction_marker

